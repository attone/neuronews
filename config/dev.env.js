var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  APP_API_URL: "'http://neuronovosti.ru/wp-json/wp/v2'"
})
