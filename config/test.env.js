var merge = require('webpack-merge')
var devEnv = require('./dev.env')

module.exports = merge(devEnv, {
  NODE_ENV: '"testing"',
  APP_API_URL: "'http://neuronovosti.ru/wp-json/wp/v2'"
})
