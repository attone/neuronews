import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/components/Home/Index'
import News from '@/components/News/Index'
import Post from '@/components/Post/Index'
import Glossary from '@/components/Glossary/Index'
import Search from '@/components/Search/Index'
import About from '@/components/About/Index'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: __dirname,
  linkActiveClass: '-active',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/category',
      name: 'category',
      component: News,
      children: [
        {
          path: 'news',
          name: 'news',
          component: News,
          children: [
            {
              path: 'page/:page/',
              name: 'news_page',
              component: News
            }
          ]
        },
        {
          path: 'article',
          name: 'articles',
          component: News,
          children: [
            {
              path: 'page/:page/',
              name: 'articles_page',
              component: News
            }
          ]
        },
        {
          path: 'event',
          name: 'events',
          component: News,
          children: [
            {
              path: 'page/:page/',
              name: 'events_page',
              component: News
            }
          ]
        },
        {
          path: 'point',
          name: 'point',
          component: News,
          children: [
            {
              path: 'page/:page/',
              name: 'point_page',
              component: News
            }
          ]
        }
      ]
    },
    {
      path: '/category/glossary',
      name: 'glossary',
      component: Glossary
    },

    // About us

    {
      path: '/team',
      name: 'about_team',
      component: About
    },
    {
      path: '/o_nas',
      name: 'about',
      component: About
    },
    {
      path: '/izviliny-2',
      name: 'about_lectury',
      component: About
    },

    // Search

    {
      path: '/search',
      name: 'search',
      component: Search,
      children: [
        {
          path: ':text',
          name: 'search_list',
          component: Search
        }
      ]
    },
    {
      path: '/:slug',
      name: 'post',
      component: Post
    },
    {
      path: '/tag',
      name: 'tags',
      component: Search,
      children: [
        {
          path: ':id',
          name: 'tag'
        }
      ]
    }
  ]
})
