import { http } from '../api'
import * as TYPES from './types'

// Категории:
// 0 - без рубрики
// 1 - Глоссарий
// 2 - Мероприятия
// 3 - Новости
// 4 - О портале
// 5 - Статьи и интервью
// 6 - Точка на карте

const paramsToString = function (params) {
  let num = 0
  switch (params.type) {
    case 'none':
      num = 1
      break
    case 'glossary':
      num = 12
      break
    case 'events':
      num = 11
      break
    case 'news':
      num = 8
      break
    case 'about':
      num = 10
      break
    case 'articles':
      num = 15
      break
    case 'point':
      num = 3
      break
    default:
      num = 0
      break
  }
  params.categories = num

  let arr = []
  for (let i in Object.keys(params)) {
    if (params[Object.keys(params)[i]] && Object.keys(params)[i] !== 'feed' && Object.keys(params)[i] !== 'type') {
      let opt = Object.keys(params)[i] + '=' + params[Object.keys(params)[i]]
      arr.push(opt)
    }
  }
  let paramsString = arr.join('&')
  return paramsString
}

const postsId = function (posts) {
  let arr = []
  for (let key in Object.keys(posts)) {
    arr.push(posts[key].id)
  }
  let parents = arr.join(',')
  return parents
}

const postsTags = function (posts) {
  let arr = []
  for (let i in posts) {
    arr = arr.concat(posts[i].tags)
  }
  let parents = arr.join(',')
  return parents
}

export default {
  async getPosts ({ commit }, params) {
    commit(TYPES.SET_LOADING, true)
    try {
      let response = await http.get(`/posts/?${paramsToString(params)}`)

      let data = response.data
      let headers = response.headers
      commit(TYPES.GET_POSTS, {data, headers, params})
      commit(TYPES.SET_LOADING, false)

      response = await http.get(`/media/?parent=${postsId(data)}&per_page=100`)
      commit(TYPES.GET_CURRENT_IMAGES, response.data)

      response = await http.get(`/tags/?include=${postsTags(data)}`)
      commit(TYPES.GET_CURRENT_TAGS, response.data)
    } catch ({ response }) {
      commit(TYPES.GET_FAIL, 'getPosts')
    }
    return commit(TYPES.SET_LOADING, false)
  },

  async getTags ({ commit }) {
    try {
      const response = await http.get('/tags/?per_page=10&orderby=count&order=desc')
      commit(TYPES.GET_TAGS, response.data)
    } catch ({ response }) {
      commit(TYPES.GET_FAIL, 'tags')
    }
  },

  async getDayPicture ({ commit }) {
    try {
      let response = await http.get('/posts/?per_page=1&tags=194')
      commit(TYPES.GET_DAY_PICTURE_BODY, response.data[0])

      response = await http.get(`/media/?parent=${response.data[0].id}`)
      commit(TYPES.GET_DAY_PICTURE_IMAGE, response.data)
    } catch ({ response }) {
      commit(TYPES.GET_FAIL, 'getDayPicture')
    }
  },

  async getDayPictureImage ({ commit }, id) {
    try {
      const response = await http.get(`/media/?parent=${id}`)
      commit(TYPES.GET_DAY_PICTURE_IMAGE, response.data)
    } catch ({ response }) {
      commit(TYPES.GET_FAIL, 'getDayPictureImage')
    }
  },

  async getMediaList ({ commit }, idList) {
    try {
      const response = await http.get(`/media/?parent=${idList}`)
      commit(TYPES.GET_CURRENT_IMAGES, response.data)
    } catch ({ response }) {
      commit(TYPES.GET_FAIL, 'getMediaList')
    }
  },

  async getTagsList ({ commit }, idList) {
    try {
      const response = await http.get(`/tags/?include=${idList}`)
      commit(TYPES.GET_CURRENT_TAGS, response.data)
    } catch ({ response }) {
      commit(TYPES.GET_FAIL, 'getTagsList')
    }
  },

  async getArticle ({ commit }, page) {
    try {
      const response = await http.get(`/posts/?slug=${page}`)
      commit(TYPES.GET_ARTICLE, response.data)
    } catch ({ response }) {
      commit(TYPES.GET_FAIL, 'getArticle')
    }
  },

  async getAbout ({ commit }, page) {
    try {
      const response = await http.get(`/posts/?slug=${page}`)
      commit(TYPES.GET_ABOUT, response.data[0])
    } catch ({ response }) {
      commit(TYPES.GET_FAIL, 'getAbout')
    }
  }
}
