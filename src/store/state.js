export default {
  news: [],
  events: [],
  articles: [],
  point: [],
  glossary: [],
  search: [],
  day_picture: {
    body: {},
    image: {}
  },
  linked: [],

  article: {
    body: {},
    images: {}
  },
  about: {
    o_nas: [],
    weare: [],
    lectury: []
  },

  page: {},

  tags: [],
  current_tags: {},
  current_images: {},
  post: {
    id: 0,
    list: {}
  },

  description: {},

  pagination: {
    news: 1,
    articles: 1,
    glossary: 1,
    events: 1,
    search: 1
  },
  loading: true
}
