import * as TYPES from './types'
import Vue from 'vue'
export default {
  [TYPES.GET_POSTS] (state, {data, headers, params}) {
    let type = params.type
    state[type] = data
    state.pagination[type] = headers['x-wp-total']
  },
  [TYPES.GET_PAGE] (state, data) {
    state[data.type] += data
  },
  [TYPES.GET_ARTICLE] (state, data) {
    data[0].content.rendered = data[0].content.rendered.replace(/(\u00a0)|(style="text-align: justify;")|(<\/*em>)/g, '')
    state.post.list = data[0]
  },
  [TYPES.GET_ABOUT] (state, data) {
    data.content.rendered = data.content.rendered.replace(/(\u00a0)|(style="text-align: justify;")|(<\/*em>)/g, '')
    state.about = data
  },
  [TYPES.GET_POSTS_LINKED] (state, data) {
    state.linked = data
  },
  [TYPES.GET_CURRENT_TAGS] (state, data) {
    for (let tag in data) {
      Vue.set(state.current_tags, data[tag].id, data[tag])
    }
  },
  [TYPES.GET_CURRENT_IMAGES] (state, data) {
    for (let i in data) {
      if (state.current_images[data[i].post] !== null) {
        Vue.set(state.current_images, data[i].post, data[i])
      }
    }
  },
  [TYPES.GET_DAY_PICTURE_BODY] (state, data) {
    state.day_picture.body = data
  },
  [TYPES.GET_DAY_PICTURE_IMAGE] (state, data) {
    state.day_picture.image = data
  },
  [TYPES.GET_CATEGORIES] (state, data) {
    state.categories = data
  },
  [TYPES.GET_TAGS] (state, data) {
    state.tags = data
  },

  [TYPES.SET_LOADING] (state, loading) {
    state.loading = loading
  },

  [TYPES.GET_FAIL] (state, text) {
    console.log('ERROR IN GET: ' + text)
  }
}
