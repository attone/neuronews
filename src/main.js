// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import { sync } from 'vuex-router-sync'
import { mapActions } from 'vuex'
import router from './router'
import store from './store'
import '../static/styles/news.css'
import '../static/styles/pagination.css'
import '../static/styles/article.css'
import '../static/styles/picture.css'
import '../static/styles/style.css'
import '../static/fonts/font-awesome/css/font-awesome.css'

import httpPlugin from './api'

sync(store, router)

Vue.use(httpPlugin, { store, router })
Vue.use(require('vue-filter'))

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {
    App
  },
  created () {
    this.getPosts({ type: 'articles' })
    this.getPosts({ type: 'news' })
    this.getTags()
  },
  methods: {
    ...mapActions(['getPosts', 'getTags'])
  }
})
